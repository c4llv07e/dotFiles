local o = vim.o
local fn = vim.fn
local options = { noremap = true }

vim.g.mapleader = ' '

vim.keymap.set('n', '<leader>pv', vim.cmd.Ex)

vim.keymap.set('n', '<c-j>',      '<c-w>j', options)
vim.keymap.set('n', '<c-h>',      '<c-w>h', options)
vim.keymap.set('n', '<c-k>',      '<c-w>k', options)
vim.keymap.set('n', '<c-l>',      '<c-w>l', options)
vim.keymap.set('n', '<esc><esc>', ':noh<return><esc>', options)
vim.keymap.set('n', '<C-n>',      ':bnext<CR>', options)
vim.keymap.set('n', '<C-p>',      ':bprevious<CR>', options)

local api = vim.api
local opts = { clear = true }

-- Highlight yanked text for a moment
local yankHl = api.nvim_create_augroup("YankHl", opts)

api.nvim_create_autocmd("TextYankPost", {
  command = "silent! lua vim.highlight.on_yank()",
  group = yankHl,
})

--vim.cmd('syntax on')
--vim.cmd('let skip_defaults_vim=1')
--vim.cmd('colorscheme murphy')
o.bs = 'indent,eol,start'
o.showcmd = true
o.ruler = true
o.mouse = 'a'
o.title = true
o.tabstop = 2
o.shiftwidth = 2
o.autoread = true
--o.pastetoggle = ''
o.splitright = true
o.splitbelow = true
o.infercase = true
o.swapfile = false
o.backup = false
o.encoding = 'utf-8'
o.wcm = fn.char2nr(api.nvim_replace_termcodes([[<TAB>]], true, true, true))
o.showmatch = false
o.number = true
o.relativenumber = true
o.colorcolumn = '75'
o.so = 999
o.expandtab = true
o.foldmethod = 'marker'
o.smartcase = true
o.inccommand= 'nosplit'
o.updatetime = 50
-- o.cursorline = true
-- o.cursorcolumn = true
o.lazyredraw = false
o.ttyfast = true
o.termguicolors = true
o.linebreak = true

o.autoindent = true
o.cindent = true

o.cinoptions='s,e0,n0,f0,{1s,}0,^-1s,L0,:s,=s,l0,b0,g0,hs,ps,ts,is,+s,c3,C0,/0,(2s,us,U0,w0,W0,m0,j0,J0,)20,*70,#0,>2'
