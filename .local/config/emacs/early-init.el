;;; early-init.el --- c4llvote's early startup script

;;; Commentary:

;;; Code:

(setq package-enable-at-startup nil)

(add-to-list 'load-path (locate-user-emacs-file "c4ll"))
(require 'c4ll_theme)

;; disable annoying ui elements
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tooltip-mode) (tooltip-mode -1))
(if (fboundp 'set-fringe-mode) (set-fringe-mode -1))
(setq visible-bell nil)

(provide 'early-init)

;;; early-init.el ends here
